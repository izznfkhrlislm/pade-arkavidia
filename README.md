# PADE (Portal Informasi Desa) 

**PADE (Portal Informasi Desa)** merupakan sistem berbasis web dan SMS, yang diharapkan dapat menjadi solusi dari permasalahan proses pengaduan masyarakat dan penyebaran informasi desa yang belum maksimal.

PADE memiliki visi untuk menjadi portal informasi desa dua arah dengan menerapkan IT untuk mewujudkan *smart* *village* di Indonesia guna menyajehterakan rakyat.

Akses halaman admin: http://arkavidia-pade.herokuapp.com/admin

End Point(s): 

```
'admin-login/' = Login as admin
'admin-logout/' = Logout from admin
'all-reports/' = Get all reports as admin
'all-announcements/' = Get all announcements as admin
'report/<str:id>/' = Get report detail by ID
'announcement/<str:id>/' = Get announcement detail by ID
'report-statuses/' = Get all report status
'unfinished-reports/' = Get all unfinished reports
'add-sms/' = Add SMS
```



Copyright Izzan Fakhril Islam, Alya Isti Safira, Priambudi Lintang Bagaskara

Tim nama adalah doa

Universitas Indonesia