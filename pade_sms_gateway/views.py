from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from twilio.twiml.messaging_response import MessagingResponse

@csrf_exempt
def send_sms(request):
    sms_response = MessagingResponse()
    sms_response.message('Selamat Datang di PADE!')
    return HttpResponse