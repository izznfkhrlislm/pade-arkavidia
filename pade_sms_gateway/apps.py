from django.apps import AppConfig


class PadeSmsGatewayConfig(AppConfig):
    name = 'pade_sms_gateway'
