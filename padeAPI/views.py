from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db import connection

from .models import *

# Create your views here.


@api_view(['POST'])
def login_as_admin(request):
    response = {}
    if request.method == 'POST':
        username = request.data['username']
        password = request.data['password']

        authenticated_user = list(Pengguna.objects.raw("SELECT * FROM admin_desa WHERE username = '%s' "
                                                       "AND password = '%s'" % (username, password)))

        if len(authenticated_user) == 1:
            request.session['username'] = username
            request.session['admin_phone_number'] = authenticated_user[0].nomor_handphone
            request.session['admin_full_name'] = authenticated_user[0].nama_lengkap
            request.session.create()

            response['message'] = 'Successfully Logged In!'
            response['username'] = request.session['username']
            response['admin_phone_number'] = request.session['admin_phone_number']
            response['admin_full_name'] = request.session['admin_full_name']
            response['session_id'] = request.session.session_key
        else:
            response['message'] = 'Wrong username and/or password'

        return Response(response)


@api_view(['POST'])
def logout_from_admin(request):
    response = {}
    if request.method == 'POST':
        if request.session.session_key is not None:
            request.session.flush()
            response['message'] = 'Successfully Logged Out!'
        else:
            response['message'] = 'You are not even logged in!'

        return Response(response)


@api_view(['GET'])
def get_all_reports_as_admin(request):
    response = {}
    if request.method == 'GET':
        all_reports_list = []
        raw_query = Pengaduan.objects.raw("SELECT * FROM pengaduan")
        response['message'] = 'Success'
        for queryset in list(raw_query):
            tracking_raw_query = TrackingPengaduan.objects.raw("SELECT * FROM tracking_pengaduan WHERE id_pengaduan = " + str(queryset.id_pengaduan))
            individual_report_map = {
                'report_id': queryset.id_pengaduan,
                'sender_phone_no': queryset.id_sms_pengaduan.nomor_handphone_pengirim.nomor_handphone,
                'sender_name': queryset.id_sms_pengaduan.nomor_handphone_pengirim.nama_lengkap,
                'report_category': queryset.kode_kategori_pengaduan.keterangan_kategori_pengaduan,
                'last_report_status': list(tracking_raw_query)[len(list(tracking_raw_query))-1].kode_status_pengaduan.keterangan_pengaduan
            }
            all_reports_list.append(individual_report_map)

        response['all_reports'] = all_reports_list

        return Response(response)


@api_view(['GET'])
def get_all_announcements_as_admin(request):
    response = {}
    if request.method == 'GET':
        all_announcements_list = []
        raw_query = Pengumuman.objects.raw("SELECT * FROM pengumuman")
        response['message'] = 'Success'
        for queryset in list(raw_query):
            individual_announcement_map = {
                'announcement_id': queryset.id_pengumuman,
                'announcement_content': queryset.id_sms_pengumuman.pesan,
                'announcement_sender_phone_no': queryset.id_sms_pengumuman.nomor_handphone_pengirim.nomor_handphone,
                'announcements_category': queryset.kode_kategori_pengumuman.keterangan_kategori_pengumuman
            }
            all_announcements_list.append(individual_announcement_map)
        response['all_announcements'] = all_announcements_list

        return Response(response)


@api_view(['GET'])
def get_report_detail_by_id(request, id):
    response = {}
    if request.method == 'GET':
        response['report_id'] = id
        raw_query = list(Pengaduan.objects.raw("SELECT * FROM pengaduan WHERE id_pengaduan = " + str(id)))[0]
        tracking_raw_query = TrackingPengaduan.objects.raw(
            "SELECT * FROM tracking_pengaduan WHERE id_pengaduan = " + str(raw_query.id_pengaduan))
        response['report_content'] = raw_query.id_sms_pengaduan.pesan
        response['report_sender_phone_no'] = raw_query.id_sms_pengaduan.nomor_handphone_pengirim.nomor_handphone
        response['report_sender_name'] = raw_query.id_sms_pengaduan.nomor_handphone_pengirim.nama_lengkap
        response['report_last_status'] = list(tracking_raw_query)[len(list(tracking_raw_query))-1].kode_status_pengaduan.keterangan_pengaduan
        response['report_category'] = raw_query.kode_kategori_pengaduan.keterangan_kategori_pengaduan
        return Response(response)


@api_view(['GET'])
def get_announcement_detail_by_id(request, id):
    response = {}
    if request.method == 'GET':
        response['announcement_id'] = id
        raw_query = list(Pengumuman.objects.raw("SELECT * FROM pengumuman WHERE id_pengumuman = " + str(id)))[0]
        response['announcement_content'] = raw_query.id_sms_pengumuman.pesan
        response['announcement_sender_phone_no'] = raw_query.id_sms_pengumuman.nomor_handphone_pengirim.nomor_handphone
        response['announcement_category'] = raw_query.kode_kategori_pengumuman.keterangan_kategori_pengumuman

        return Response(response)


@api_view(['GET'])
def get_all_report_status(request):
    response = {}
    all_report_statuses = []
    if request.method == 'GET':
        raw_query = list(StatusPengaduan.objects.raw("SELECT * FROM status_pengaduan"))
        for queryset in list(raw_query):
            report_map = {
                queryset.kode_pengaduan: queryset.keterangan_pengaduan
            }
            all_report_statuses.append(report_map)

        response['all_report_statuses'] = all_report_statuses

        return Response(response)


@api_view(['GET'])
def get_all_unfinished_reports(request):
    response = {}
    if request.method == 'GET':
        response['message'] = 'Success',
        all_unfinished_reports = []
        raw_query = TrackingPengaduan.objects.raw("SELECT * FROM tracking_pengaduan WHERE kode_status_pengaduan = 'WFA'")
        for queryset in list(raw_query):
            individual_report_map = {
                'report_id': queryset.id_pengaduan.id_pengaduan,
                'sender_phone_no': queryset.id_sms.nomor_handphone_pengirim.nomor_handphone,
                'sender_name': queryset.id_sms.nomor_handphone_pengirim.nama_lengkap,
                'report_category': queryset.kode_kategori_pengaduan.keterangan_kategori_pengaduan,
            }
            all_unfinished_reports.append(individual_report_map)

        response['all_unfinished_reports'] = all_unfinished_reports

        return Response(response)


@api_view(['POST'])
def update_report_status(request):
    response = {}
    if request.method == 'POST':
        new_report_status_code = request.data['new_report_status_code']
        report_id = request.data['report_id']

        report_validation = len(list(Pengaduan.objects.raw("SELECT * FROM pengaduan WHERE id_pengaduan = %s" % report_id)))
        if report_validation == 0:
            response['status'] = "Failed"
            response['message'] = "Invalid report ID"
            return Response(response)
        else:
            report_status_validation = len(list(StatusPengaduan.objects.raw("SELECT * FROM status_pengaduan WHERE kode_pengaduan = '%s'" % new_report_status_code)))
            if report_status_validation == 0:
                response['status'] = 'Failed'
                response['message'] = "Invalid Report Code"
                return Response(response)
            else:
                valid_report_entity = list(Pengaduan.objects.raw("SELECT * FROM pengaduan WHERE id_pengaduan = %s" % report_id))[0]
                id_sms = valid_report_entity.id_sms_pengaduan.id_sms
                phone_no = valid_report_entity.id_sms_pengaduan.nomor_handphone_pengirim.nomor_handphone
                report_category = valid_report_entity.kode_kategori_pengaduan.kode_kategori_pengaduan

                with connection.cursor() as c:
                    report_update_sql = "UPDATE pengaduan SET kode_status_pengaduan = '%s' WHERE id_pengaduan = %s" % (new_report_status_code, report_id)
                    add_new_tracking_sql = "INSERT INTO tracking_pengaduan (id_pengaduan, id_sms, nomor_handphone_pengirim, kode_kategori_pengaduan, kode_status_pengaduan) VALUES (%s, %s, '%s', '%s', '%s')" % (report_id, id_sms, phone_no, report_category, new_report_status_code)
                    c.execute(report_update_sql)
                    c.execute(add_new_tracking_sql)

                response['message'] = 'Success'

        return Response(response)


@api_view(['POST'])
def add_sms_message(request):
    response = {}
    if request.method == 'POST':
        sender_phone_no = request.data['phone_no']
        text = request.data['text']
        msg_type = request.data['msg_type']

        latest_sms_id = list(PesanSms.objects.raw("SELECT * FROM pesan_sms ORDER BY id_sms DESC"))[0].id_sms
        new_sms_id = latest_sms_id + 1

        # User validation
        user_validation = len(list(Pengguna.objects.raw("SELECT * FROM pengguna WHERE nomor_handphone = '%s'" % sender_phone_no)))
        if user_validation == 0:
            response['status'] = "Invalid user"
            return Response(response)
        else:
            with connection.cursor() as c:
                insert_sms_data = "INSERT INTO pesan_sms (id_sms, pesan, kode_tipe_pesan_sms, nomor_handphone_pengirim) VALUES (%s,'%s','%s','%s')" % (new_sms_id, text, msg_type, sender_phone_no)
                c.execute(insert_sms_data)

            if msg_type == "PND": # Pengaduan
                pengaduan_category = request.data['pengaduan_category']
                latest_pengaduan_id = list(Pengaduan.objects.raw("SELECT * FROM pengaduan ORDER BY id_pengaduan DESC"))[0].id_pengaduan
                new_pengaduan_id = latest_pengaduan_id + 1
                with connection.cursor() as c:
                    pengaduan_sql = "INSERT INTO pengaduan (id_pengaduan, id_sms_pengaduan, kode_kategori_pengaduan, kode_status_pengaduan) VALUES (%s, %s, '%s', 'WFA')" % (new_pengaduan_id, new_sms_id, pengaduan_category)
                    tracking_pengaduan_sql = "INSERT INTO tracking_pengaduan (id_pengaduan, id_sms, kode_kategori_pengaduan, kode_status_pengaduan, nomor_handphone_pengirim) VALUES (%s, %s, '%s', 'WFA', '%s')" % (new_pengaduan_id, new_sms_id, pengaduan_category, sender_phone_no)
                    c.execute(pengaduan_sql)
                    c.execute(tracking_pengaduan_sql)

            elif msg_type == "PGU": # Pengumuman
                pengumuman_category = request.data['pengumuman_category']
                latest_pengumuman_id = list(Pengumuman.objects.raw("SELECT * FROM pengumuman ORDER BY id_pengumuman DESC"))[0].id_pengumuman
                new_pengumuman_id = latest_pengumuman_id + 1
                with connection.cursor() as c:
                    pengumuman_sql = "INSERT INTO pengumuman (id_pengumuman, id_sms_pengumuman, kode_kategori_pengumuman) VALUES (%s, %s, '%s')" % (new_pengumuman_id, new_sms_id, pengumuman_category)
                    c.execute(pengumuman_sql)

            else:
                response['status'] = 'Failed'
                response['error_message'] = "Unknown message type!"
                return Response(response)

        response['status'] = "Success"
        return Response(response)


@api_view(['GET'])
def get_stats(request):
    response = {}
    if request.method == 'GET':
        response['message'] = 'Success'
        report_categories = KategoriPengaduan.objects.raw("SELECT * FROM kategori_pengaduan")
        announcement_categories = KategoriPenugmuman.objects.raw("SELECT * FROM kategori_pengumuman")

        all_reports = len(list(Pengaduan.objects.raw("SELECT * FROM pengaduan")))
        all_announcements = len(list(Pengumuman.objects.raw("SELECT * FROM pengumuman")))

        reports = []
        announcements = []

        for queryset in list(report_categories):
            specific_report_category = len(list(Pengaduan.objects.raw("SELECT * FROM pengaduan WHERE kode_kategori_pengaduan = '%s'" % queryset.kode_kategori_pengaduan)))
            specific_report_category_map = {
                queryset.kode_kategori_pengaduan: specific_report_category
            }
            reports.append(specific_report_category_map)

        for queryset in list(announcement_categories):
            specific_announcement_category = len(list(Pengumuman.objects.raw("SELECT * FROM pengumuman WHERE kode_kategori_pengumuman = '%s'" % queryset.kode_kategori_pengumuman)))
            specific_announcement_category_map = {
                queryset.kode_kategori_pengumuman: specific_announcement_category
            }
            announcements.append(specific_announcement_category_map)

        response['reports'] = reports
        response['announcements'] = announcements
        response['total'] = all_reports + all_announcements

        return Response(response)


@api_view(['GET'])
def get_reports_list_by_phone_number(request, phone_no):
    response = {}
    if request.method == 'GET':
        all_reports_list = []
        response['message'] = 'Success'
        raw_query = Pengaduan.objects.raw("SELECT * FROM pengaduan p, pesan_sms ps, pengguna p2 where p.id_sms_pengaduan = ps.id_sms and ps.nomor_handphone_pengirim = p2.nomor_handphone and p2.nomor_handphone = '%s'" % phone_no)

        for queryset in list(raw_query):
            individual_reports_map = {
                'report_id': queryset.id_pengaduan,
                'report_category': queryset.kode_kategori_pengaduan.keterangan_kategori_pengaduan,
                'report_status': queryset.kode_status_pengaduan.keterangan_pengaduan
            }
            all_reports_list.append(individual_reports_map)

        response['reports'] = all_reports_list
        return Response(response)
