from django.core.management.base import BaseCommand
from django.conf import Settings
from padeAPI.models import (Pengguna, AdminDesa, StatusPengaduan, KategoriPengaduan, TipePesanSms, KategoriPenugmuman,
                            PesanSms, Pengaduan, Pengumuman, TrackingPengaduan)
from django.db import IntegrityError

dummy_datas = {
    "Izzan Fakhril Islam": [
        "081213141516",
        "Jalan Bahagia no. 1 Desa Sukamaju",
    ],
    "Alya Isti Safira": [
        "089667515857",
        "Jalan Senang no. 2 Desa Sukamaju",
    ],
    "Priambudi Lintang Bagaskara": [
        "082112344321",
        "Jalan Cinta no. 3 Desa Sukamaju"
    ],
    "John Doe": [
        "+17027664886",
        "Jalan Sayang no. 4 Desa Sukamaju"
    ]
}

status_pengaduan_datas = {
    "WFA": "Menunggu Persetujuan Admin",
    "REJ": "Ditolak Admin",
    "PRO": "Diproses Admin",
    "ACC": "Disetujui Admin",
    "FIN": "Selesai"
}

sms_types_datas = {
    "PND": "Pengaduan",
    "PGU": "Pengumuman"
}

kategori_pengaduan_datas = {
    "ADM": "Administrasi",
    "BPN": "Bantuan Pangan",
    "BCN": "Bencana",
    "DD": "Dana Desa",
    "DRT": "Darurat",
    "GOV": "Informasi Pemerintahan",
    "INF": "Infrastruktur",
    "JSO": "Jaminan Sosial",
    "KSH": "Kesehatan",
    "KSR": "Kesejahteraan Rakyat",
    "KOM": "Komunikasi",
    "LIH": "Lingkungan Hidup",
    "MSB": "Musibah",
    "PRW": "Pariwisata",
    "TAX": "Pajak",
    "PLB": "Pelayanan Barang",
    "EDU": "Pendidikan",
    "BNK": "Perbankan",
    "IZN": "Perizinan",
    "AGR": "Pertanian",
    "SDA": "Sumber Daya Alam",
    "STK": "Situasi Khusus",
    "LIV": "Tempat Tinggal",
    "USH": "Usaha",
    "OTR": "Lainnya"
}

kategori_pengumuman_datas = {
    "ACU": "Acara Umum",
    "BB": "Buka Bersama",
    "HBH": "Halal Bi Halal",
    "KRJ": "Kerja Bakti",
    "LMB": "Lomba",
    "MBR": "Makan Bersama",
    "PLT": "Pelatihan",
    "PMB": "Pembinaan",
    "PKD": "Pemilihan Kepala Desa",
    "PND": "Pengembangan Desa",
    "PGJ": "Pengajian",
    "PNY": "Penyambutan",
    "PRY": "Perayaan",
    "RKO": "Rapat Koordinasi",
    "SOS": "Sosialisasi",
    "SYK": "Syukuran"
}

sms_msg_dummy_datas = {
    '1': [
        "081213141516",
        "Jalanan depan rumah rusak parah, sudah 5 bulan gaada perbaikan.",
        "PND"
    ],
    '2': [
        "081213141516",
        "Saluran got mampet di RT 10, akibatnya banjir kalau hujan deras.",
        "PND"
    ],
    '3': [
        "082112344321",
        "Mohon bantuannya, sudah 3 bulan desa kekeringan dan kesulitan air bersih.",
        "PND"
    ],
    '4': [
        "089667515857",
        "Akan ada imunisasi virus corona untuk warga RW 25 pada hari Minggu tanggal 9 Februari 2020.",
        "PGU"
    ]
}

pengaduan_dummy_datas = {
    '1': [
        1,
        "INF",
        "PRO"
    ],
    '2': [
        2,
        "INF",
        "WFA"
    ],
    '3': [
        3,
        "BCN",
        "WFA"
    ]
}

pengumuman_dummy_datas = {
    '1': [
        4,
        "ACU",
    ]
}

tracking_pengaduan_dummy_datas = {
    #Key: ID SMS
    '1': [
        1, #id pengaduan
        "081213141516", #nomor hape
        "INF", #kode kategori
        "PRO" #kode status
    ],
    '2': [
        2,
        "081213141516",
        "INF",
        "WFA"
    ],
    '3': [
        3,
        "082112344321",
        "BCN",
        "WFA"
    ]
}


class Command(BaseCommand):

    def handle(self, *args, **options):
        # Create dummy admin account
        print("Creating Dummy Admin Desa account...")
        AdminDesa.objects.create(
            username='desa-suka-maju',
            password='desasukamaju123',
            nomor_handphone='+19196481600',
            nama_lengkap='Sutaryo',
        )

        print("Creating dummy Pengguna account...")
        # Create dummy Pengguna account
        for dummy_name in list(dummy_datas.keys()):
            Pengguna.objects.create(
                nomor_handphone=dummy_datas[dummy_name][0],
                nama_lengkap=dummy_name,
                alamat_rumah=dummy_datas[dummy_name][1]
            )

        print("Creating Status Pengaduan Entry...")
        for status_pengaduan_code in list(status_pengaduan_datas.keys()):
            StatusPengaduan.objects.create(
                kode_pengaduan=status_pengaduan_code,
                keterangan_pengaduan=status_pengaduan_datas[status_pengaduan_code]
            )

        print("Creating Kategori Pengaduan Entry...")
        for kategori_pengaduan_code in list(kategori_pengaduan_datas.keys()):
            KategoriPengaduan.objects.create(
                kode_kategori_pengaduan=kategori_pengaduan_code,
                keterangan_kategori_pengaduan=kategori_pengaduan_datas[kategori_pengaduan_code]
            )

        print("Creating Kategori Pengumuman Entry...")
        for kategori_pengumuman_code in list(kategori_pengumuman_datas.keys()):
            KategoriPenugmuman.objects.create(
                kode_kategori_pengumuman=kategori_pengumuman_code,
                keterangan_kategori_pengumuman=kategori_pengumuman_datas[kategori_pengumuman_code]
            )

        print("Create Tipe Pesan SMS Entry...")
        for sms_entry_code in list(sms_types_datas.keys()):
            TipePesanSms.objects.create(
                kode_tipe=sms_entry_code,
                keterangan_tipe_pesan_sms=sms_types_datas[sms_entry_code]
            )

        print("Creating SMS Text Dummy...")
        for id_sms in list(sms_msg_dummy_datas.keys()):
            PesanSms.objects.create(
                id_sms=int(id_sms),
                pesan=sms_msg_dummy_datas[id_sms][1],
                nomor_handphone_pengirim=list(Pengguna.objects.filter(nomor_handphone=sms_msg_dummy_datas[id_sms][0]))[0],
                kode_tipe_pesan_sms=list(TipePesanSms.objects.filter(kode_tipe=sms_msg_dummy_datas[id_sms][2]))[0]
            )

        print("Creating Pengaduan dummy data...")
        for pengaduan_id in list(pengaduan_dummy_datas.keys()):
            Pengaduan.objects.create(
                id_pengaduan=int(pengaduan_id),

                id_sms_pengaduan=list(PesanSms.objects.filter(
                    id_sms=pengaduan_dummy_datas[pengaduan_id][0]))[0],

                kode_kategori_pengaduan=list(KategoriPengaduan.objects.filter(
                    kode_kategori_pengaduan=pengaduan_dummy_datas[pengaduan_id][1]))[0],

                kode_status_pengaduan=list(StatusPengaduan.objects.filter(
                    kode_pengaduan=pengaduan_dummy_datas[pengaduan_id][2]))[0]
            )

        print("Creating Pengumuman dummy data...")
        for pengumuman_id in list(pengumuman_dummy_datas.keys()):
            Pengumuman.objects.create(
                id_pengumuman=int(pengumuman_id),
                id_sms_pengumuman=list(PesanSms.objects.filter(
                    id_sms=pengumuman_dummy_datas[pengumuman_id][0]))[0],
                kode_kategori_pengumuman=list(KategoriPenugmuman.objects.filter(
                    kode_kategori_pengumuman=pengumuman_dummy_datas[pengumuman_id][1]))[0]
            )

        print("Creating Tracking Pengaduan dummy data...")
        for sms_id in list(tracking_pengaduan_dummy_datas.keys()):
            TrackingPengaduan.objects.create(
                id_pengaduan=Pengaduan.objects.filter(id_pengaduan=tracking_pengaduan_dummy_datas[sms_id][0])[0],
                id_sms=PesanSms.objects.filter(id_sms=int(sms_id))[0],
                kode_kategori_pengaduan=KategoriPengaduan.objects.filter(kode_kategori_pengaduan=tracking_pengaduan_dummy_datas[sms_id][2])[0],
                nomor_handphone_pengirim=Pengguna.objects.filter(nomor_handphone=tracking_pengaduan_dummy_datas[sms_id][1])[0],
                kode_status_pengaduan=StatusPengaduan.objects.filter(kode_pengaduan=tracking_pengaduan_dummy_datas[sms_id][3])[0]
            )
