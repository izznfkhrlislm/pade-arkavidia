from django.urls import path
from .views import login_as_admin, logout_from_admin, get_all_reports_as_admin, get_all_announcements_as_admin, \
    get_report_detail_by_id, get_announcement_detail_by_id, get_all_report_status, get_all_unfinished_reports, \
    add_sms_message, get_stats, update_report_status, get_reports_list_by_phone_number

urlpatterns = [
    path('admin-login/', login_as_admin, name='admin-login'),
    path('admin-logout/', logout_from_admin, name='admin-logout'),
    path('all-reports/', get_all_reports_as_admin, name='all-reports'),
    path('all-announcements/', get_all_announcements_as_admin, name='all-announcements'),
    path('report/<str:id>/', get_report_detail_by_id, name='report-detail'),
    path('announcement/<str:id>/', get_announcement_detail_by_id, name='announcement-detail'),
    path('report-statuses/', get_all_report_status, name='report-statuses'),
    path('unfinished-reports/', get_all_unfinished_reports, name='unfinished-reports'),
    path('add-sms/', add_sms_message, name='add-sms'),
    path('stats/', get_stats, name='stats'),
    path('update-report-status/', update_report_status, name='update-report-status'),
    path('reports/<str:phone_no>/', get_reports_list_by_phone_number, name='reports'),
]