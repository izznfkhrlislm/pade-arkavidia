from django.db import models


class Pengguna(models.Model):
    nomor_handphone = models.CharField(primary_key=True, max_length=50, null=False)
    nama_lengkap = models.CharField(max_length=255, null=False)
    alamat_rumah = models.CharField(max_length=255, null=False)

    class Meta:
        db_table = 'pengguna'


class AdminDesa(models.Model):
    username = models.CharField(primary_key=True, max_length=50, null=False)
    password = models.CharField(max_length=100, null=False)
    nomor_handphone = models.CharField(max_length=255, null=False)
    nama_lengkap = models.CharField(max_length=255, null=False)

    class Meta:
        db_table = 'admin_desa'
        unique_together = (('username', 'nomor_handphone'),)


class StatusPengaduan(models.Model):
    kode_pengaduan = models.CharField(primary_key=True, max_length=5, null=False)
    keterangan_pengaduan = models.CharField(max_length=50, null=False)

    class Meta:
        db_table = 'status_pengaduan'


class KategoriPengaduan(models.Model):
    kode_kategori_pengaduan = models.CharField(primary_key=True, max_length=5, null=False)
    keterangan_kategori_pengaduan = models.CharField(max_length=50, null=False)

    class Meta:
        db_table = 'kategori_pengaduan'


class TipePesanSms(models.Model):
    kode_tipe = models.CharField(primary_key=True, max_length=5, null=False)
    keterangan_tipe_pesan_sms = models.CharField(max_length=50, null=False)

    class Meta:
        db_table = 'tipe_pesan_sms'


class KategoriPenugmuman(models.Model):
    kode_kategori_pengumuman = models.CharField(primary_key=True, max_length=5, null=False)
    keterangan_kategori_pengumuman = models.CharField(max_length=50, null=False)

    class Meta:
        db_table = 'kategori_pengumuman'


class PesanSms(models.Model):
    id_sms = models.IntegerField(primary_key=True)
    nomor_handphone_pengirim = models.ForeignKey('Pengguna', models.DO_NOTHING, db_column='nomor_handphone_pengirim')
    pesan = models.TextField()
    kode_tipe_pesan_sms = models.ForeignKey('TipePesanSms', models.DO_NOTHING, db_column='kode_tipe_pesan_sms')

    class Meta:
        db_table = 'pesan_sms'
        unique_together = (('id_sms', 'nomor_handphone_pengirim'),)


class Pengaduan(models.Model):
    id_pengaduan = models.IntegerField(primary_key=True)
    id_sms_pengaduan = models.ForeignKey('PesanSms', models.DO_NOTHING, db_column='id_sms_pengaduan')
    kode_kategori_pengaduan = models.ForeignKey('KategoriPengaduan', models.DO_NOTHING, db_column='kode_kategori_pengaduan')
    kode_status_pengaduan = models.ForeignKey('StatusPengaduan', models.DO_NOTHING, db_column='kode_status_pengaduan')

    class Meta:
        db_table = 'pengaduan'
        unique_together = (('id_pengaduan', 'id_sms_pengaduan'),)


class Pengumuman(models.Model):
    id_pengumuman = models.IntegerField(primary_key=True)
    id_sms_pengumuman = models.ForeignKey('PesanSms', models.DO_NOTHING, db_column='id_sms_pengumuman')
    kode_kategori_pengumuman = models.ForeignKey('KategoriPenugmuman', models.DO_NOTHING, db_column='kode_kategori_pengumuman')

    class Meta:
        db_table = 'pengumuman'
        unique_together = (('id_pengumuman', 'id_sms_pengumuman'),)


class TrackingPengaduan(models.Model):
    id_pengaduan = models.ForeignKey('Pengaduan', models.DO_NOTHING, db_column='id_pengaduan')
    id_sms = models.ForeignKey('PesanSms', models.DO_NOTHING, db_column='id_sms')
    nomor_handphone_pengirim = models.ForeignKey('Pengguna', models.DO_NOTHING, db_column='nomor_handphone_pengirim')
    kode_kategori_pengaduan = models.ForeignKey('KategoriPengaduan', models.DO_NOTHING, db_column='kode_kategori_pengaduan')
    kode_status_pengaduan = models.ForeignKey('StatusPengaduan', models.DO_NOTHING, db_column='kode_status_pengaduan')

    class Meta:
        db_table = 'tracking_pengaduan'
        unique_together = (('id_pengaduan', 'kode_status_pengaduan'),)